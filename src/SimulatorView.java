import java.awt.*;
import javax.imageio.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.swing.*;
import java.util.HashMap;

/**
 * A graphical view of the simulation grid. The view displays a colored
 * rectangle for each location representing its contents. It uses a default
 * background color. Colors for each type of species can be defined using the
 * setColor method.
 * 
 * @author David J. Barnes and Michael Kolling
 * @version 2006.03.30
 * @modified Chuck Cusack, 2007.09.14
 * @modified Jason Gombas, 2018, January
 * 
 */
public class SimulatorView extends JPanel {
	// Colors used for empty locations.
	private static final Color EMPTY_COLOR = Color.white;

	// Color used for objects that have no defined color.
	private static final Color UNKNOWN_COLOR = Color.gray;

	private final String STEP_PREFIX = "Step: ";
	private final String POPULATION_PREFIX = "Population: ";
	private JLabel stepLabel, population;
	private FieldView fieldView;
	private boolean imageCouldntLoad;

	// A map for storing colors for participants in the simulation
	private HashMap<Class, Color> colors;
	// A statistics object computing and storing simulation information
	private FieldStats stats;
	// Create a field for the images for each actor.
	BufferedImage rabitImg;
	BufferedImage foxImg;
	BufferedImage infectedImg;
	BufferedImage megatronImg;

	/**
	 * Create a view of the given width and height.
	 * 
	 * @param height
	 *            The simulation's height.
	 * @param width
	 *            The simulation's width.
	 */
	public SimulatorView(int height, int width) {
		imageCouldntLoad = false;
		stats = new FieldStats();
		colors = new HashMap<Class, Color>();

		stepLabel = new JLabel(STEP_PREFIX, JLabel.CENTER);
		population = new JLabel(POPULATION_PREFIX, JLabel.CENTER);

		fieldView = new FieldView(height, width);

		// Set the layout manager for this JPanel
		this.setLayout(new BorderLayout());

		// Add the labels and view of the field to this panel
		this.add(stepLabel, BorderLayout.NORTH);
		this.add(fieldView, BorderLayout.CENTER);
		this.add(population, BorderLayout.SOUTH);

		// Load all of the images in the bin directory into
		// the BufferedImage fields.
		try {
			rabitImg = ImageIO.read(this.getClass().getResource("rabit.png"));
			foxImg = ImageIO.read(this.getClass().getResource("fox.png"));
			infectedImg = ImageIO.read(this.getClass().getResource("death-skull.png"));
			megatronImg = ImageIO.read(this.getClass().getResource("decepticon-icon.png"));
		} catch (IOException e) {
			e.printStackTrace();
			imageCouldntLoad = true;
		}
	}

	/**
	 * Define a color to be used for a given class of animal.
	 * 
	 * @param animalClass
	 *            The animal's Class object.
	 * @param color
	 *            The color to be used for the given class.
	 */
	public void setColor(Class actorClass, Color color) {
		colors.put(actorClass, color);
	}

	/**
	 * @return The color to be used for a given class of animal.
	 */
	private Color getColor(Class actorClass) {
		Color col = colors.get(actorClass);
		if (col == null) {
			// no color defined for this class
			return UNKNOWN_COLOR;
		} else {
			return col;
		}
	}

	/**
	 * Show the current status of the field.
	 * 
	 * @param step
	 *            Which iteration step it is.
	 * @param field
	 *            The field whose status is to be displayed.
	 */
	public void showStatus(int step, Field field) {
		if (!isVisible())
			setVisible(true);

		stepLabel.setText(STEP_PREFIX + step);
		stats.reset();

		fieldView.preparePaint();
		// Loops over the grid and draws marks for each actor onto the grid.
		for (int row = 0; row < field.getDepth(); row++) {
			for (int col = 0; col < field.getWidth(); col++) {
				Actor actor = field.getActorAt(row, col);
				if (actor != null) {
					stats.incrementCount(actor.getClass());
					fieldView.drawMark(col, row, getColor(actor.getClass()), actor.getClass());
				} else {
					fieldView.drawMark(col, row, EMPTY_COLOR, null);
				}
			}
		}
		stats.countFinished();

		population.setText(POPULATION_PREFIX + stats.getPopulationDetails(field));
		fieldView.repaint();
	}

	/**
	 * Determine whether the simulation should continue to run.
	 * 
	 * @return true If there is more than one species alive.
	 */
	public boolean isViable(Field field) {
		return stats.isViable(field);
	}

	/**
	 * Provide a graphical view of a rectangular field. This is a nested class (a
	 * class defined inside a class) which defines a custom component for the user
	 * interface. This component displays the field.
	 */
	private class FieldView extends JPanel {
		private final int GRID_VIEW_SCALING_FACTOR = 6;

		private int gridWidth, gridHeight;
		private int xScale, yScale;
		Dimension size;
		private Graphics g;
		private Image fieldImage;

		/**
		 * Create a new FieldView component.
		 */
		public FieldView(int height, int width) {
			gridHeight = height;
			gridWidth = width;
			size = new Dimension(0, 0);
		}

		/**
		 * Tell the GUI manager how big we would like to be.
		 */
		public Dimension getPreferredSize() {
			return new Dimension(gridWidth * GRID_VIEW_SCALING_FACTOR, gridHeight * GRID_VIEW_SCALING_FACTOR);
		}

		/**
		 * Prepare for a new round of painting. Since the component may be resized,
		 * compute the scaling factor again.
		 */
		public void preparePaint() {
			if (!size.equals(getSize())) { // if the size has changed...
				size = getSize();
				fieldImage = fieldView.createImage(size.width, size.height);
				g = fieldImage.getGraphics();

				xScale = size.width / gridWidth;
				if (xScale < 1) {
					xScale = GRID_VIEW_SCALING_FACTOR;
				}
				yScale = size.height / gridHeight;
				if (yScale < 1) {
					yScale = GRID_VIEW_SCALING_FACTOR;
				}
			}
		}

		/**
		 * Paint on grid location on this field in a given color. This will either paint
		 * a box with the actor's color, or it will draw the image loaded for that actor
		 */
		public void drawMark(int x, int y, Color color, Class actorClass) {
			if (imageCouldntLoad) {
				g.setColor(color);
				g.fillRect(x * xScale, y * yScale, xScale - 1, yScale - 1);
			} else {
				// The scaling and resizing are done to fit each 'icon'
				// into the grid.
				if (actorClass == Fox.class) {
					g.drawImage(foxImg, x * xScale, y * yScale, xScale - 1, yScale - 1, color, null);
				} else if (actorClass == Rabbit.class) {
					g.drawImage(rabitImg, x * xScale, y * yScale, xScale - 1, yScale - 1, color, null);
				} else if (actorClass == Infected.class) {
					g.drawImage(infectedImg, x * xScale, y * yScale, xScale - 1, yScale - 1, color, null);
				} else if (actorClass == Megatron.class) {
					g.drawImage(megatronImg, x * xScale, y * yScale, xScale - 1, yScale - 1, color, null);
				} else {
					g.setColor(color);
					g.fillRect(x * xScale, y * yScale, xScale - 1, yScale - 1);
				}
			}
		}

		/**
		 * The field view component needs to be redisplayed. Copy the internal image to
		 * screen.
		 */
		public void paintComponent(Graphics g) {
			if (fieldImage != null) {
				Dimension currentSize = getSize();
				if (size.equals(currentSize)) {
					g.drawImage(fieldImage, 0, 0, null);
				} else {
					// Rescale the previous image.
					g.drawImage(fieldImage, 0, 0, currentSize.width, currentSize.height, null);
				}
			}
		}
	}
}
