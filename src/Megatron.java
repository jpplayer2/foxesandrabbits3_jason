import java.util.Iterator;

/**
 * Megatrons teleport every actor in their vicinity to the location 0,0. He has
 * such advanced technology that we don't even know this is possible. He walks
 * around randomly after he teleports everyone around him.
 * 
 * @author Jason Gombas
 * @version 2018.01.24
 */
public class Megatron extends Actor {

	@Override
	public void act(Field currentField, Field updatedField) {
		// Get all the adjacent locations
		Iterator<Location> it = currentField.adjacentLocations(getLocation());
		while (it.hasNext()) {
			Location location = it.next();
			Actor actor = currentField.getActorAt(location);
			// check if there is no actor in the location
			if (actor != null) {
				actor.setLocation(0, 0);
			}
		}
		Location newLocation = updatedField.freeAdjacentLocation(getLocation());
		// Only transfer to the updated field if there was a free location
		if (newLocation != null) {
			setLocation(newLocation);
			updatedField.place(this);
		} else {
			updatedField.place(this);
		}
	}
}
