import java.util.Random;

/**
 * The Infected never die. They infect all animals, and only animals. They
 * 'reach' out to one of the adjacent actors and attempt to infect. There is a
 * probability that they will infect. They cannot move.
 * 
 * @author Jason Gombas
 * @version 2018.01.24
 */
public class Infected extends Actor {
	// How likely will the infected infect.
	private static final double INFECTION_PROBABILITY = 0.5;
	private static final Random rand = new Random();

	/**
	 * Attempt to infect one of the adjacent actors. Only infects animals, and has a
	 * probability of infecting.
	 */
	public void act(Field currentField, Field updatedField) {
		Location newOne = currentField.occupiedAdjacentLocation(getLocation());
		if (newOne != null) {
			Actor occupant = currentField.getActorAt(newOne);
			if (occupant instanceof Animal && rand.nextDouble() <= INFECTION_PROBABILITY) {
				infect((Animal) occupant, updatedField);
			}
		}
		// The infected cannot move so put them in the same place
		// in the updated field.
		updatedField.place(this);
	}

	/**
	 * This method infects an animal. The animal becomes infected and the animal
	 * dies :(
	 * 
	 * @param animal
	 * @param updatedField
	 */
	private void infect(Animal animal, Field updatedField) {
		Infected newInfected = new Infected();
		newInfected.setLocation(animal.getLocation());
		updatedField.place(newInfected);
		animal.setDead();
	}
}
