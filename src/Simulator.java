import java.util.Random;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collections;
import java.awt.Color;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * A simulation which currently includes 4 actors. There are 4 actors currently
 * implemented. Megatrons Foxes Rabbits Infected. Each actor is contained in a
 * field and after each step they perform their 'act'.
 * 
 * @author David J. Barnes and Michael Kolling
 * @version 2006.03.30
 * @modified Chuck Cusack, September, 2007
 * @modified Jason Gombas, January, 2018
 * 
 */
public class Simulator {
	// Constants representing configuration information for the simulation.
	// The default width for the grid.
	private static final int DEFAULT_WIDTH = 50;
	// The default depth of the grid.
	private static final int DEFAULT_DEPTH = 50;
	// The probability that a fox will be created in any given grid position.
	private static final double FOX_CREATION_PROBABILITY = 0.02;
	// The probability that a rabbit will be created in any given grid position.
	private static final double RABBIT_CREATION_PROBABILITY = 0.08;

	// The current state of the field.
	private Field field;
	// A second field, used to build the next stage of the simulation.
	private Field updatedField;
	// The current step of the simulation.
	private int step;
	// A graphical view of the simulation.
	private SimulatorView view;
	// Added this variable for use by the thread.
	private int numberSteps;

	// Added the following GUI stuff
	// The main window to display the simulation (and your buttons, etc.).
	JButton runOneButton;
	private JFrame mainFrame;

	/**
	 * Construct a simulation field with default size.
	 */
	public Simulator() {
		this(DEFAULT_DEPTH, DEFAULT_WIDTH);
	}

	public static void main(String[] args) {
		// Create the simulator
		Simulator s = new Simulator();
	}

	/**
	 * Create a simulation field with the given size.
	 * 
	 * @param depth
	 *            Depth of the field. Must be greater than zero.
	 * @param width
	 *            Width of the field. Must be greater than zero.
	 */
	public Simulator(int depth, int width) {
		if (width <= 0 || depth <= 0) {
			System.out.println("The dimensions must be greater than zero.");
			System.out.println("Using default values.");
			depth = DEFAULT_DEPTH;
			width = DEFAULT_WIDTH;
		}
		field = new Field(depth, width);
		updatedField = new Field(depth, width);

		// Create a view of the state of each location in the field.
		view = new SimulatorView(depth, width);
		view.setColor(Rabbit.class, Color.orange);
		view.setColor(Fox.class, Color.blue);
		view.setColor(Infected.class, Color.BLACK);
		view.setColor(Megatron.class, Color.CYAN);

		mainFrame = new JFrame();

		mainFrame.setTitle("Fox and Rabbit Simulation");

		// Add window listener so it closes properly.
		// when the "X" in the upper right corner is clicked.
		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		// Add a button to run 1 steps.
		runOneButton = new JButton("Run 1 step");
		runOneButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				simulateOneStep();
			}
		});
		// Adding things to a JFrame first requires that you get the
		// content pane. Notice you don't do with with a JPanel.
		Container contents = mainFrame.getContentPane();
		contents.add(view, BorderLayout.CENTER);
		contents.add(runOneButton, BorderLayout.NORTH);

		mainFrame.pack();
		reset();
		mainFrame.setVisible(true);
	}

	/**
	 * Run the simulation from its current state for a reasonably long period, e.g.
	 * 500 steps. This is currently not implemented but will be used in future
	 * versions.
	 */
	public void runLongSimulation() {
		simulate(500);
	}

	/**
	 * Run the simulation from its current state for the given number of steps. Stop
	 * before the given number of steps if it ceases to be viable. This has been
	 * modified so it uses a thread--this allows it to work in conjunction with
	 * Swing. Modified by Chuck Cusack, Sept 18, 2007
	 * 
	 * @param numSteps
	 *            How many steps to run for.
	 */
	public void simulate(int numSteps) {
		numberSteps = numSteps;
		// Create a thread
		Thread runThread = new Thread() {
			// When the thread runs, it will simulate numberSteps steps.
			public void run() {
				// Disable the button until the simulation is done.
				runOneButton.setEnabled(false);
				for (int step = 1; step <= numberSteps && view.isViable(field); step++) {
					simulateOneStep();
				}
				// Now re-enable the button
				runOneButton.setEnabled(true);
			}
		};
		// Start the thread
		runThread.start();
		// Now this method exits, allowing the GUI to update. The simulation is being
		// run on a different thread, so the GUI updates as the simulation continues.
	}

	/**
	 * Run the simulation from its current state for a single step. Iterate over the
	 * whole field updating the state of each fox and rabbit.
	 */
	public void simulateOneStep() {
		step++;
		// let all actors act
		ArrayList<Actor> actors = field.getActors();
		Collections.shuffle(actors); // to randomize the order they act.
		for (Iterator<Actor> it = actors.iterator(); it.hasNext();) {
			Actor actor = it.next();
			actor.act(field, updatedField);
		}

		// Swap the field and updatedField at the end of the step.
		Field temp = field;
		field = updatedField;
		updatedField = temp;
		updatedField.clear();

		// Display the new field on screen.
		view.showStatus(step, field);
	}

	/**
	 * Reset the simulation to a starting position.
	 */
	public void reset() {
		step = 0;
		field.clear();
		updatedField.clear();
		populate(field);

		// Show the starting state in the view.
		view.showStatus(step, field);
	}

	/**
	 * Populate a field with foxes and rabbits.
	 * 
	 * @param field
	 *            The field to be populated.
	 */
	private void populate(Field field) {
		Random rand = new Random();
		field.clear();
		for (int row = 0; row < field.getDepth(); row++) {
			for (int col = 0; col < field.getWidth(); col++) {
				// Put in a random amount of foxes
				if (rand.nextDouble() <= FOX_CREATION_PROBABILITY) {
					Fox fox = new Fox(true);
					fox.setLocation(row, col);
					field.place(fox);
				}
				// put in a random amount of rabbits
				else if (rand.nextDouble() <= RABBIT_CREATION_PROBABILITY) {
					Rabbit rabbit = new Rabbit(true);
					rabbit.setLocation(row, col);
					field.place(rabbit);
				}
				// else leave the location empty.
			}
		}
		// Populate one Infected right in the middle
		int colInfected = (int) Math.round(field.getDepth() / 2.);
		int widInfected = (int) Math.round(field.getWidth() / 2.);
		Infected infected = new Infected();
		infected.setLocation(colInfected, widInfected);
		field.place(infected);

		// Populate 2 megatrons, at 1/4 the height and width of the
		// current field and one at 3/4 the height and width of the
		// current field.
		int colMegatron1 = (int) Math.round(field.getDepth() / 4.);
		int widMegatron1 = (int) Math.round(field.getWidth() / 4.);
		int colMegatron2 = (int) Math.round(3 * field.getDepth() / 4.);
		int widMegatron2 = (int) Math.round(3 * field.getWidth() / 4.);
		Megatron megatron1 = new Megatron();
		Megatron megatron2 = new Megatron();
		megatron1.setLocation(colMegatron1, widMegatron1);
		megatron2.setLocation(colMegatron2, widMegatron2);
		field.place(megatron1);
		field.place(megatron2);
	}
}
