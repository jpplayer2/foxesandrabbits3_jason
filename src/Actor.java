/**
 * In order to be an actor, you must be able to act. All actors have a location
 * and anyone can get their location and set their location.
 * 
 * @author Jason Gombas
 * @version 2018.01.24
 *
 */
public abstract class Actor {
	// The only field required in this abstract class.
	private Location location;

	/**
	 * Make this actor act - that is: make it do whatever it wants/needs to do.
	 * 
	 * @param currentField
	 *            The field currently occupied.
	 * @param updatedField
	 *            The field to transfer to.
	 */
	abstract public void act(Field currentField, Field updatedField);

	/**
	 * Return the actor's location.
	 * 
	 * @return The actor's location.
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * Set the actor's location.
	 * 
	 * @param row
	 *            The vertical coordinate of the location.
	 * @param col
	 *            The horizontal coordinate of the location.
	 */
	public void setLocation(int row, int col) {
		this.location = new Location(row, col);
	}

	/**
	 * Set the actor's location.
	 * 
	 * @param location
	 *            The actor's location.
	 */
	public void setLocation(Location location) {
		this.location = location;
	}

}